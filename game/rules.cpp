//
// Created by Марк on 30.05.19.
//

#include "Game.hpp"
#include "../Configs.hpp"




function<void(Bonus *, Player *, GameManager * gm)> bpr1 = [](Bonus * b, Player * p, GameManager * gm) {
    if (isIntersect(b, p)) {
        b->effect(gm);
        gm->getBonuses().erase(find(gm->getBonuses().begin(), gm->getBonuses().end(), b));
        delete b;
    }
};
function<void(Circle * c, Level * b, GameManager * gm)> cmr1 = [](Circle * c, Level * b, GameManager * gm) {
    pair<Brick *, Vector2f> res = b->isInterset(c);
    if (res.first == nullptr) return;
    c->getSpeed().x *= res.second.x;
    c->getSpeed().y *= res.second.y;
    c->response(SIMPLE_DELTA_TIME);
    res.first->isShocked();
    if (res.first->getIsAlive() == 0) {
        Bonus * bonus = res.first->getBonus();
        if (!bonus) return;
        gm->addBonus(bonus);
        bonus->alive();
    }
};
function<void(Circle * c, Player * p, GameManager * gm)> cpr1 = [](Circle * c, Player * p, GameManager * gm) {
    switch (isIntersect(p, c)) {
    case 1:
        c->getSpeed().y *= -1;
        c->response(SIMPLE_DELTA_TIME);
        break;
    case -1:
        c->getSpeed().x *= -1;
        c->response(SIMPLE_DELTA_TIME);
        break;
}

};
function<void(Circle *, GameManager * gm)> cr1 = [](Circle * c, GameManager * gm) {
    Vector2f & pos = c->getPos();
    Vector2f & speed = c->getSpeed();
    float R = c->getR();
    if (pos.x - R < 0) {
        pos.x = R;
        speed.x *= -1;
    } else if (pos.x + R > gm->getWinSize().x) {
        pos.x = gm->getWinSize().x - R;
        speed.x *= -1;
    }
    if (pos.y - R < 0) {
        pos.y = R;
        speed.y *= -1;
    } else if (pos.y + R > gm->getWinSize().y) {
        pos.y = gm->getWinSize().y - R;
        speed.y *= -1;
        (gm->getPlayer()).getLives()--;
        gm->getCircles().erase(find(gm->getCircles().begin(), gm->getCircles().end(), c));
        delete c;
    }
};
function<void(Player *, GameManager * gm)> pr1 = [](Player * p, GameManager * gm) {
    if (p->getLives() < 0) {
        gm->endGame();
    }
};