//
// Created by Марк on 24.05.19.
//
#pragma once
#include <vector>
#include <SFML/Graphics.hpp>
#include "../Anim.hpp"
#include "../objects/bricks.hpp"

#ifndef ARCANOID_SCREEN_HPP
#define ARCANOID_SCREEN_HPP



using namespace std;
using namespace sf;



class GameScreen : public Screen {
    GameManager * gm;

public:
    GameScreen(Vector2f winSize);
    void render(double delta, RenderWindow * rw) override ;
    void response(double delta) override ;
};



class GameManager {
    Vector2f winSize;
    Object * player;
    vector<Object *> circles;
    vector<Object *> bonuses;
    Object * level;

    vector<function<void(Circle *, Level *, GameManager * gm)>> CMrules;
    vector<function<void(Circle *, GameManager * gm)>> Crules;
    vector<function<void(Player *, GameManager * gm)>> Prules;
    vector<function<void(Circle *, Player *, GameManager * gm)>> CPrules;
    vector<function<void(Bonus *, Player *, GameManager * gm)>> BPrules;
public:
    vector<function<void(GameManager * gm)>> BonusRules;

    GameManager(Vector2f s);
    void render(double delta, RenderWindow * rw);
    void response(double delta);
    Vector2f getWinSize();
    void addBonus(Bonus * b) { bonuses.push_back(b); }
    Player & getPlayer() { return *(Player *)player; }
    vector<Object *> & getCircles() { return circles; }
    vector<Object *> & getBonuses() { return bonuses; }
    void endGame();
};



extern function<void(Circle * c, Level * b, GameManager * gm)> cmr1;
extern function<void(Circle * c, Player * p, GameManager * gm)> cpr1;
extern function<void(Circle *, GameManager * gm)> cr1;
extern function<void(Player *, GameManager * gm)> pr1;
extern function<void(Bonus *, Player *, GameManager * gm)> bpr1;
#endif //ARCANOID_SCREEN_HPP
