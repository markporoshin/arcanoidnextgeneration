//
// Created by Марк on 24.05.19.
//

#include <iostream>
#include "Game.hpp"
#include "../Configs.hpp"

GameScreen::GameScreen(Vector2f winSize) {
    gm = new GameManager(winSize);
}
Vector2f GameManager::getWinSize() {
    return winSize;
}
void GameScreen::render(double delta, RenderWindow *rw) {
    gm->render(delta, rw);
}
void GameScreen::response(double delta) {
    gm->response(delta);
}



void GameManager::render(double delta, RenderWindow * rw) {
    player->render(delta, rw);
    for (Object * circle : circles)
        circle->render(delta, rw);
    level->render(delta, rw);
    for (Object * bonus : bonuses)
        bonus->render(delta, rw);
}
void GameManager::response(double delta) {
    player->response(delta);
    for (Object * circle : circles)
        circle->response(delta);
    level->response(delta);
    for (Object * bonus : bonuses)
        bonus->response(delta);


    for (auto rule : BonusRules)
        rule(this);
    for (Object * c : circles) {
        for(auto rule : Crules)
            rule((Circle *)c, this);
        for(auto rule : CPrules)
            rule((Circle *)c, (Player *)player, this);
        for(auto rule : CMrules)
            rule((Circle *)c, (Level *)level, this);
    }
    for (auto rule : Prules)
        rule((Player *)player, this);
    for (Object * b : bonuses) {
        for (auto rule: BPrules)
            rule((Bonus *)b, (Player *)player, this);
    }
}
GameManager::GameManager(Vector2f s) : winSize(s) {
    Vector2f playerPos(s.x * 0.5f, s.y - PLAYER_OFFSET);
    Vector2f playerSize(PLAYER_SIZE_X, PLAYER_SIZE_Y);
    player = new Player(playerPos, playerSize, {PLAYER_SPEED_X, PLAYER_SPEED_Y}, 1);

    Vector2f circlePos(s.x * 0.5f, s.y - BRICK_OFFSET);
    circles.push_back(new Circle(CIRCLE_R, circlePos, {CIRCLE_SPEED_X, CIRCLE_SPEED_Y}));
    level = new Level("/Users/mark/CLionProjects/arcanoid/resources/level1.txt",
                      "/Users/mark/CLionProjects/arcanoid/resources/bonus1.txt",
                      s);

    Crules.push_back(cr1);
    Prules.push_back(pr1);
    CPrules.push_back(cpr1);
    CMrules.push_back(cmr1);
    BPrules.push_back(bpr1);
}
void GameManager::endGame() {
    std::cout << "END GAME" << std::endl;
}




