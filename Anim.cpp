//
// Created by Марк on 24.05.19.
//

#include <iostream>
#include "Anim.hpp"

using namespace std;

Anim::Anim(unsigned sizeX, unsigned sizeY, string title) : winSize(sizeX, sizeY) {
    window = new RenderWindow(sf::VideoMode(sizeX, sizeY), title);
    window->setVerticalSyncEnabled(true);
    window->setFramerateLimit(60);

}

void Anim::setScreen(Screen * screen) {
    curScreen = screen;
}

double Anim::getDeltaTime() {
    return curTime - lastTime;
}

void Anim::run() {
    while (window->isOpen())
    {
        sf::Event event;
        while (window->pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window->close();
        }

        window->clear(sf::Color::White);
        render();
        window->display();
    }
}

void Anim::render() {
    double time = ((double)clock.getElapsedTime().asMicroseconds()) / 1000000.0;
    clock.restart();
    double gameDeltaTime = time;
    curScreen->response(gameDeltaTime);
    curScreen->render(gameDeltaTime, window);
    lastTime = curTime;
}

Vector2f Anim::getWinSize() {
    return winSize;
}