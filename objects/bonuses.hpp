//
// Created by Марк on 09.06.19.
//
#pragma once
#include "objects.hpp"

#ifndef ARCANOID_BONUSES_HPP
#define ARCANOID_BONUSES_HPP


class GameManager;

class Bonus : public Object {
protected:
    bool isAlive;
    float R;
    CircleShape * out;
    CircleShape * in;
public:
    Bonus(float R, Vector2f pos, Vector2f speed);
    void alive() { isAlive = true; };
    void render(double delta, RenderWindow * rw) override ;
    float const & getR() { return R; }
    void response(double delta) override;

    virtual void effect(GameManager * gm) {}
    virtual ~Bonus() {}
};


class SimpleBonus : public Bonus {
public:
    SimpleBonus(float R, Vector2f pos, Vector2f speed = {0, 0});
    void effect(GameManager * gm) override {}
    ~SimpleBonus() override {
        delete out;
        delete in;
    }
    /*void response(double delta) override {
        Bonus::response(delta);
    }
    void render(double delta, RenderWindow * rw) override {
        Bonus::render(delta, rw);
    }*/
};

class SuperSpeedBonus : public Bonus {
public:
    SuperSpeedBonus(float R, Vector2f pos, Vector2f speed = {0, 0});

    void effect(GameManager *gm) override;
};

class SuperSlowBonus : public Bonus {
public:
    SuperSlowBonus(float R, Vector2f pos, Vector2f speed = {0, 0});

    void effect(GameManager *gm) override;
};

class PlayerSpeedUpBonus : public Bonus {
public:
    PlayerSpeedUpBonus(float R, Vector2f pos, Vector2f speed = {0, 0});
    void effect(GameManager *gm) override;
};

class PlayerSpeedDownBonus : public Bonus {
public:
    PlayerSpeedDownBonus(float R, Vector2f pos, Vector2f speed = {0, 0});
    void effect(GameManager *gm) override;
};

class OneJumpBonus : public Bonus {
public:
    OneJumpBonus(float R, Vector2f pos, Vector2f speed = {0, 0});
    void effect(GameManager *gm) override;
};

class GlueBonus : public Bonus {
    int N; //number of glues
public:
    GlueBonus(float R, Vector2f pos, Vector2f speed = {0, 0}, int N = 1);
    void effect(GameManager *gm) override;
};


class IncrizeBouns : public Bonus {

};
#endif //ARCANOID_BONUSES_HPP
