//
// Created by Марк on 25.05.19.
//

#ifndef ARCANOID_OBJECTS_HPP
#define ARCANOID_OBJECTS_HPP

#include <SFML/Graphics.hpp>
#include <cmath>
#include "../math/vec2.hpp"

using namespace sf;
using namespace std;

class Bonus;


class Object {
protected:
    Vector2f pos;
    Vector2f speed;
public:
    Object(Vector2f pos, Vector2f speed) : pos(pos), speed(speed) {}
    virtual void render(double delta, RenderWindow * rw) = 0;
    virtual void response(double delta) = 0;
    virtual ~Object() = default;

    Vector2f & getPos() {return pos;}
    Vector2f & getSpeed() {return speed;}
};

class Circle : public Object {
    double R;
public:
    CircleShape * shape;
    Circle(double R, Vector2f pos, Vector2f speed);
    void render(double delta, RenderWindow * rw) override;
    void response(double delta) override;
    double getR() { return R; }
    ~Circle() override;
};

class Player : public Object {
    Vector2f sides;
    int lives;
public:
    RectangleShape * shape;
    Player(Vector2f pos, Vector2f sides, Vector2f speed, unsigned lives);
    void render(double delta, RenderWindow * rw) override;
    void response(double delta) override;
    Vector2f & getSides() { return sides;}
    int & getLives() { return  lives;}
    ~Player() override;
};






int isIntersect(Object * o1, Object * o2, bool isSwaped = false);
int isIntersetCirclePlayer(Circle * c, Player * p);
int isIntersetBonusPlayer(Bonus * c, Player * p);


//class Brick : public Object {
//
//};



#endif //ARCANOID_OBJECTS_HPP
