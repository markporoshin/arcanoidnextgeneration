//
// Created by Марк on 09.06.19.
//

#include <iostream>
#include "bonuses.hpp"
#include "../game/Game.hpp"
#include "../Configs.hpp"


Bonus::Bonus(float R, Vector2f pos, Vector2f speed) : Object(pos, speed), R(R), isAlive(false) {
    out = new CircleShape(R);
    out->setOutlineThickness(3);
    out->setOrigin(R, R);

    in = new CircleShape(R / 2);
    out->setOrigin(R / 2, R / 2);
}
void Bonus::render(double delta, RenderWindow * rw) {
    out->setPosition(pos.x - R / 2, pos.y - R / 2);
    in->setPosition(pos.x - R / 2, pos.y - R / 2);
    rw->draw(*out);
    rw->draw(*in);
}

void Bonus::response(double delta) {
    if (isAlive) {
        pos.x += delta * speed.x;
        pos.y += delta * speed.y;
    }
}


SimpleBonus::SimpleBonus(float R, Vector2f pos, Vector2f speed) : Bonus(R, pos, speed) {
    out->setOutlineColor(sf::Color::Black);
    in->setFillColor(sf::Color::Black);
}


SuperSpeedBonus::SuperSpeedBonus(float R, Vector2f pos, Vector2f speed) : Bonus(R, pos, speed){
    in->setFillColor(sf::Color::Blue);
}
void SuperSpeedBonus::effect(GameManager *gm) {
    auto circles = gm->getCircles();
    for (Object * c : circles) {
        c->getSpeed().x *= 2;
        c->getSpeed().y *= 2;
    }
}


SuperSlowBonus::SuperSlowBonus(float R, Vector2f pos, Vector2f speed) : Bonus(R, pos, speed) {
    in->setFillColor(sf::Color::Magenta);
}
void SuperSlowBonus::effect(GameManager *gm) {
    auto circles = gm->getCircles();
    for (Object * c : circles) {
        c->getSpeed().x /= 2;
        c->getSpeed().y /= 2;
    }
}


PlayerSpeedUpBonus::PlayerSpeedUpBonus(float R, Vector2f pos, Vector2f speed) : Bonus(R, pos, speed) {
    in->setFillColor(sf::Color::Yellow);
}
void PlayerSpeedUpBonus::effect(GameManager *gm) {
    gm->getPlayer().getSpeed().x *= 2;
}


PlayerSpeedDownBonus::PlayerSpeedDownBonus(float R, Vector2f pos, Vector2f speed) : Bonus(R, pos, speed) {
    in->setFillColor(sf::Color::Green);
}
void PlayerSpeedDownBonus::effect(GameManager * gm) {
    gm->getPlayer().getSpeed().x /= 2;
}


OneJumpBonus::OneJumpBonus(float R, Vector2f pos, Vector2f speed) : Bonus(R, pos, speed) {
    out->setOutlineColor(sf::Color::Black);
    out->setOutlineThickness(3);
    in->setFillColor(sf::Color::Green);
}
void OneJumpBonus::effect(GameManager * gm) {
    int ind = gm->BonusRules.size();
    function<void(GameManager * gm)> oneJumpRule = [ind](GameManager * gm) {

        for (Object * o : gm->getCircles()) {
            //std::cout << o->getPos().x << " " << o->getPos().y << std::endl;
            Circle * c = (Circle *)o;
            if (c->getPos().y + c->getR() > gm->getWinSize().y) {
                c->getPos().y = gm->getWinSize().y - c->getR();
                c->getSpeed().y *= -1;
                c->response(SIMPLE_DELTA_TIME);
                gm->BonusRules.erase(gm->BonusRules.begin() + ind);
                break;
            }
        }
    };
    gm->BonusRules.push_back(oneJumpRule);
}


GlueBonus::GlueBonus(float R, Vector2f pos, Vector2f speed, int N) : Bonus(R, pos, speed), N(N) {
    out->setOutlineColor(sf::Color::Black);
    out->setOutlineThickness(3);
    in->setFillColor(sf::Color(123, 45, 200));
}
void GlueBonus::effect(GameManager *gm) {
    Bonus::effect(gm);
    int ind = gm->BonusRules.size();
    Circle ** caughted = new Circle*(nullptr);
    bool * isCaughted = new bool(false);
    float * offset = new float(0);
    int * N = new int(this->N);
    function<void(GameManager * gm)> oneJumpRule = [ind, N, caughted, isCaughted, offset](GameManager * gm) {


        if (*isCaughted) {
            (*caughted)->getPos().x = gm->getPlayer().getPos().x - *offset;
            if(Keyboard::isKeyPressed(Keyboard::W)) {
                (*caughted)->getSpeed().y = CIRCLE_SPEED_Y;
                (*caughted)->getSpeed().x = rand() % CIRCLE_SPEED_LIMITS - CIRCLE_SPEED_X;
                if ((*N)-- == 0) {
                    gm->BonusRules.erase(gm->BonusRules.begin() + ind);
                    delete caughted;
                    delete isCaughted;
                    delete N;
                }
                *isCaughted = false;
            }
        } else {
            for (Object * o : gm->getCircles()) {
                Circle * c = (Circle *)o;
                if (isIntersetCirclePlayer(c, &gm->getPlayer())) {
                    (*caughted) = c;
                    (*isCaughted) = true;
                    (*offset) = gm->getPlayer().getPos().x - (*caughted)->getPos().x;
                    (*caughted)->getPos().y = gm->getPlayer().getPos().y - gm->getPlayer().getSides().y;
                    (*caughted)->getSpeed() = {0, 0};
                    break;
                }
            }
        }

    };
    gm->BonusRules.push_back(oneJumpRule);
}







