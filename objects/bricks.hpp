//
// Created by Марк on 08.06.19.
//

#ifndef ARCANOID_BRICKS_HPP
#define ARCANOID_BRICKS_HPP

#include <SFML/Graphics.hpp>
#include "objects.hpp"
#include "bonuses.hpp"

using namespace std;


class Brick : public Object {
protected:
    Bonus * bonus;
    Vector2f sides;
    int lives;
    bool active;
public:
    RectangleShape * shape;
    Brick(Vector2f pos, Vector2f size);
    void setBonus(Bonus * bonus) { this->bonus = bonus; }
    Bonus * getBonus() { return bonus; }
    void render(double delta, RenderWindow * rw) override;
    void response(double delta) override;
    Vector2f & getSides() { return sides; }
    int & getIsAlive() { return lives; }
    virtual Bonus * isShocked() {
        lives--;
        if (lives == 0) return bonus;
        return nullptr;
    }
    bool & isActive() { return active; }
    ~Brick() override;
};

class IndestructibleBrick : public Brick {
public:
    IndestructibleBrick(Vector2f pos, Vector2f size) : Brick(pos, size) {
        shape->setFillColor(sf::Color::Black);
    }
    Bonus * isShocked() override { return nullptr; }
};

class ShimmerBrick : public Brick {

public:
    ShimmerBrick(Vector2f pos, Vector2f size) : Brick(pos, size) { shape->setOutlineColor(sf::Color::Green); }
    void response(double delta) override;
};

class DuringBrick : public Brick {
    double colorCoef;
public:
    DuringBrick(Vector2f pos, Vector2f size, int lives) : Brick(pos, size) {
        colorCoef = 256 / (lives+2);
        this->lives = lives;
        shape->setFillColor(sf::Color(256 - (lives+1) * colorCoef, 256 - (lives+1) * colorCoef, 256 - (lives+1) * colorCoef));
    }
    Bonus * isShocked() override {
        Brick::isShocked();
        shape->setFillColor(sf::Color(256 - (lives + 1) * colorCoef, 256 - (lives + 1) * colorCoef, 256 - (lives + 1) * colorCoef));
        return nullptr;
    }
};

class ShiftyBrick : public Brick {
protected:
    Vector2f from;
    Vector2f to;
public:
    ShiftyBrick(Vector2f size, Vector2f from, Vector2f to, float speed) : Brick(from, size), from(from), to(to) {
        this->speed = Vector2f(speed, speed);
        shape->setOutlineColor(sf::Color::Red);
    }

    void response(double delta) override;
};

class Level : public Object {
    vector<Object *> * map;
public:
    pair<Brick *, Vector2f> isInterset(Circle *p);
    Level(string brickDescription, string bonusDescription, Vector2f winSize);
    void render(double delta, RenderWindow * rw) override;
    void response(double delta) override;
    ~Level() override;
};


#endif //ARCANOID_BRICKS_HPP
