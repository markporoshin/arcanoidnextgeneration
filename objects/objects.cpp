//
// Created by Марк on 25.05.19.
//

#include <iostream>
#include <fstream>
#include "objects.hpp"
#include "bricks.hpp"


using namespace sf;
using namespace std;

int isIntersetBonusPlayer(Bonus * c, Player * p) {
    double d = sqrt((c->getPos().x - p->getPos().x) * (c->getPos().x - p->getPos().x) + (c->getPos().y - p->getPos().y) * (c->getPos().y - p->getPos().y));


    if (fabs(c->getPos().x - p->getPos().x) < (c->getR() + p->getSides().x / 2) &&
        fabs(c->getPos().y - p->getPos().y) < (c->getR() + p->getSides().y / 2)) {
        if (p->getPos().x - p->getSides().x / 2 < c->getPos().x &&
            p->getPos().x + p->getSides().x / 2 > c->getPos().x)
            return 1;
        return -1;
    }
    return 0;
}
int isIntersetCirclePlayer(Circle * c, Player * p) {
    double d = sqrt((c->getPos().x - p->getPos().x) * (c->getPos().x - p->getPos().x) + (c->getPos().y - p->getPos().y) * (c->getPos().y - p->getPos().y));


    if (fabs(c->getPos().x - p->getPos().x) < (c->getR() + p->getSides().x / 2) &&
        fabs(c->getPos().y - p->getPos().y) < (c->getR() + p->getSides().y / 2)) {
        if (p->getPos().x - p->getSides().x / 2 < c->getPos().x &&
            p->getPos().x + p->getSides().x / 2 > c->getPos().x)
            return 1;
        return -1;
    }
    return 0;
}
int isIntersect(Object * o1, Object * o2, bool isSwaped) {
    if (dynamic_cast<Circle *>(o1) && dynamic_cast<Player *>(o2)) {
        return isIntersetCirclePlayer((Circle *)o1, (Player *)o2);
    } else if(dynamic_cast<Bonus *>(o1) && dynamic_cast<Player *>(o2)) {
        return isIntersetBonusPlayer((Bonus *)o1, (Player *)o2);
    }else if (!isSwaped) {
        return isIntersect(o2, o1, true);
    }
    return 0;
}


Circle::Circle(double R, Vector2f pos, Vector2f speed) : Object(pos, speed), R(R) {
    shape = new CircleShape(R);
    shape->setOrigin(R, R);
    shape->setOutlineColor(sf::Color::Black);
    shape->setOutlineThickness(4);
}
void Circle::render(double delta, RenderWindow * rw) {

    shape->setPosition(pos);
    rw->draw(*shape);
}
Circle::~Circle() {
    delete shape;
}
void Circle::response(double delta) {
    Vector2f curSpeed(speed.x * delta, speed.y * delta);
    pos = pos + curSpeed;
}




Player::Player(Vector2f pos, Vector2f sides, Vector2f speed, unsigned lives) : Object(pos, speed), sides(sides), lives(lives) {
    shape = new RectangleShape(pos);
    shape->setOrigin(sides.x/2, sides.y/2);
    shape->setSize(sides);
    shape->setOutlineColor(sf::Color::Black);
    shape->setOutlineThickness(4);
}
void Player::response(double delta) {
    if (Keyboard::isKeyPressed(Keyboard::Left)) {
        pos.x -= speed.x * delta;
    }
    if (Keyboard::isKeyPressed(Keyboard::Right)) {
        pos.x += speed.x * delta;
    }

}
void Player::render(double delta, RenderWindow *rw) {
    shape->setPosition(pos);
    rw->draw(*shape);
}
Player::~Player() {
    delete shape;
}














