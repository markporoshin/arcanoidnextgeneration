//
// Created by Марк on 08.06.19.
//

#include <fstream>
#include "bricks.hpp"

static int isIntersetCircleBrick(Circle * c, Brick * p) {

    double d = sqrt((c->getPos().x - p->getPos().x) * (c->getPos().x - p->getPos().x) + (c->getPos().y - p->getPos().y) * (c->getPos().y - p->getPos().y));


    if (fabs(c->getPos().x - p->getPos().x) < (c->getR() + p->getSides().x / 2) &&
        fabs(c->getPos().y - p->getPos().y) < (c->getR() + p->getSides().y / 2)) {
        //cout << "collision";
        if (p->getPos().x - p->getSides().x / 2 < c->getPos().x &&
            p->getPos().x + p->getSides().x / 2 > c->getPos().x) {
            //cout << " y" << endl;

            return 1;
        }
        //cout << " x" << endl;
        return -1;
    }
    return 0;
}
Brick::Brick(Vector2f pos, Vector2f size) : Object(pos, {0, 0}), sides(size), lives(1), active(true), bonus(nullptr) {
    shape = new RectangleShape(pos);
    shape->setOrigin(sides.x / 2, sides.y / 2);
    shape->setSize(sides);
    shape->setOutlineColor(sf::Color::Black);
    shape->setOutlineThickness(4);
}
void Brick::render(double delta, RenderWindow *rw) {
    shape->setPosition(pos);
    rw->draw(*shape);
    if (bonus) {
        bonus->getPos() = this->getPos();
        bonus->render(delta, rw);
    }
}
void Brick::response(double delta) {
    if (bonus) bonus->response(delta);
}
Brick::~Brick() {
    delete shape;
}

void ShimmerBrick::response(double delta) {
    Brick::response(delta);
    if (((float)rand()) / RAND_MAX > 0.98f) {
        isActive() ? isActive() = false : isActive() = true;
    }
}

void ShiftyBrick::response(double delta)  {
#define E 10
    Brick::response(delta);
    Vector2f curDist(to.x - pos.x, to.y - pos.y);
    double dist = sqrt(curDist.x * curDist.x + curDist.y * curDist.y);
    pos.x += curDist.x / dist * delta * speed.x;
    pos.y += curDist.y / dist * delta * speed.y;

    if (fabs(to.x - pos.x) + fabs(to.y - pos.y) < E) {
        Vector2f buf = to;
        to = from;
        from = buf;
        pos = from;
    }
}

static Vector2f getPosByCoord(int i, int j, int N, int M, Vector2f winSize) {
    float sizeX = winSize.x  / N;
    float sizeY = winSize.y / 2 / M;

    return {sizeX * (i + 0.5f), sizeY * (j + 0.5f)};
}
static Bonus * createBonus(char type, Brick * b, ifstream & in) {
    float R = min(b->getSides().x, b->getSides().y) * 0.1f;
    switch (type) {
        case 'S': {
            return new SimpleBonus(R, b->getPos(), {0, 200});
        }
        case 'U': {
            return new SuperSpeedBonus(R, b->getPos(), {0, 200});
        }
        case 'D': {
            return new SuperSlowBonus(R, b->getPos(), {0, 200});
        }
        case 'P': {
            return new PlayerSpeedUpBonus(R, b->getPos(), {0, 200});
        }
        case 'p': {
            return new PlayerSpeedDownBonus(R, b->getPos(), {0, 200});
        }
        case 'J': {
            return new OneJumpBonus(R, b->getPos(), {0, 200});
        }
        case 'G': {
            int N;
            in >> N;
            return new GlueBonus(R, b->getPos(), {0, 200}, N);
        }
    }
    return nullptr;
}
Level::Level(string brickDescription, string bonusDescription, Vector2f winSize) : Object({0, 0}, {0, 0}) {
    unsigned N, M;
    char brickType, bonusType;
    ifstream bricks(brickDescription);
    ifstream bonuses(bonusDescription);
    bricks >> M >> N;

    map = new vector<Object *>(M * N);
    for (int i = 0; i < M; ++i) {
        for (int j = 0; j < N; ++j) {
            bonuses >> bonusType;
            bricks >> brickType;
            if (brickType == 'B') {
                Object * o = new Brick(getPosByCoord(j, i, N, M, winSize),
                                       {winSize.x / N - 10, winSize.y  / 2 / M  - 10});
                ((Brick *)o)->setBonus(createBonus(bonusType, ((Brick *)o), bonuses));
                (*map)[i * N + j] = o;
            } else if (brickType == 'I') {
                Object * o = new IndestructibleBrick(getPosByCoord(j, i, N, M, winSize),
                                       {winSize.x / N  - 10, winSize.y  / 2 / M  - 10});
                ((Brick *)o)->setBonus(createBonus(bonusType, ((Brick *)o), bonuses));
                (*map)[i * N + j] = o;
            } else if (brickType == 'S') {
                Object * o = new ShimmerBrick(getPosByCoord(j, i, N, M, winSize),
                                                     {winSize.x / N  - 10, winSize.y  / 2 / M  - 10});
                ((Brick *)o)->setBonus(createBonus(bonusType, ((Brick *)o), bonuses));
                (*map)[i * N + j] = o;
            } else if (brickType == 'D') {
                int lives;
                bricks >> lives;
                Object * o = new DuringBrick(getPosByCoord(j, i, N, M, winSize),
                                              {winSize.x / N  - 10, winSize.y  / 2 / M  - 10}, lives);
                ((Brick *)o)->setBonus(createBonus(bonusType, ((Brick *)o), bonuses));
                (*map)[i * N + j] = o;
            } else if (brickType == 'R') {
                float speed;
                Vector2f from, to;
                bricks >> from.x >> from.y >> to.x >> to.y >> speed;

                from = getPosByCoord(from.x, from.y, N, M, winSize);
                to = getPosByCoord(to.x, to.y, N, M, winSize);

                Object * o = new ShiftyBrick({winSize.x / N  - 10, winSize.y  / 2 / M  - 10}, from, to, speed);
                ((Brick *)o)->setBonus(createBonus(bonusType, ((Brick *)o), bonuses));
                (*map)[i * N + j] = o;
            }

        }
    }
}
void Level::render(double delta, RenderWindow *rw) {
    for (Object * o : *map) {
        if (o == nullptr || !((Brick *)o)->getIsAlive() || !((Brick *)o)->isActive()) continue;
        o->render(delta, rw);
    }
}
void Level::response(double delta) {
    for (Object * o : *map) {
        if (o == NULL) continue;
        if (!((Brick *)o)->getIsAlive()) continue;
        o->response(delta);
    }
}
pair<Brick *, Vector2f> Level::isInterset(Circle *p) {
    int i;
    for (Object * o : *map) {
        if (o == nullptr || !((Brick *)o)->getIsAlive() ||  !((Brick *)o)->isActive()) continue;
        if((i = isIntersetCircleBrick(p, (Brick *)o))) {
            if (i == 1)
                return {(Brick *)o, {1, -1}};
            return {(Brick *)o, {-1, 1}};
        }
    };
    return {nullptr, {0, 0}};
}
Level::~Level() {
    for (Object * o : *map) {
        delete o;
    }
    delete map;
}




