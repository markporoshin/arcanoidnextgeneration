//
// Created by Марк on 24.05.19.
//
#pragma once
#ifndef ARCANOID_ANIM_HPP
#define ARCANOID_ANIM_HPP

#include <vector>
#include <list>
#include <SFML/Graphics.hpp>
#include "math/vec2.hpp"
#include "objects/objects.hpp"

using namespace std;
using namespace sf;

class Anim;

class Screen {
protected:

public:
    Screen() {};
    virtual void render(double delta, RenderWindow * rw) = 0;
    virtual void response(double delta) = 0;
    //void addObj(Object * obj);
};

class Anim {
    Clock clock;
    RenderWindow * window;
    Screen * curScreen;
    double curTime;
    double lastTime;
    Vector2f winSize;
    void render();
public:
    Anim(unsigned sizeX, unsigned sizeY, string title);
    void setScreen(Screen * screen);
    void run();
    double getDeltaTime();
    Vector2f getWinSize();
};


#endif //ARCANOID_ANIM_HPP

















