#include <iostream>
#include "Anim.hpp"
#include "game/Game.hpp"






int main() {
    Anim anim(1000, 1000, "game");
    GameScreen game(anim.getWinSize());
    anim.setScreen(&game);
    anim.run();
    return 0;
}