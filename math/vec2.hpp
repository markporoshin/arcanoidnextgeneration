//
// Created by Марк on 25.05.19.
//

#ifndef ARCANOID_VEC2_HPP
#define ARCANOID_VEC2_HPP


struct vec2 {
    double x;
    double y;

    vec2(double x, double y) : x(x), y(y) {};

    vec2 operator+(vec2 const &);
    vec2 operator*(double k);
    void rotate(double alpha);
};


#endif //ARCANOID_VEC2_HPP
