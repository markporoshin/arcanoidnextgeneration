//
// Created by Марк on 25.05.19.
//

#include <cmath>
#include "vec2.hpp"



vec2 vec2::operator+(vec2 const & b) {
    return {x+b.x, y+b.y};
}

vec2 vec2::operator*(double k) {
    return {x * k, y * k};
}

void vec2::rotate(double alpha) {
    x =   x * cos(alpha) + y * sin(alpha);
    y = - x * sin(alpha) + y * cos(alpha);
}
